# Code by Joey Casamento, algorithm by Matt Malczyk

from __future__ import division
import music21 as music
from collections import defaultdict, deque
from operator import itemgetter
import pprint
import random
import sys


class Markov:
    def __init__(self, order, *sources):
        self.matrix = defaultdict(lambda: defaultdict(lambda: 0))
        self.order = order
        self._prob_grid = None
        
        for s in sources:
            self.train(s)

    def train(self, stream):
        if type(stream) == str:
            stream = music.converter.parse(stream)

        features = filter(lambda x: x, [self.get_features(n) for n in stream.flat])

        for evt in range(self.order, len(features)):
            x, y = (tuple(features[evt-self.order:evt]), features[evt])
            self.matrix[x][y] += 1
            self._prob_grid = None

    def to_dict(self):
        d = {}
        for x, row in self.matrix.iteritems():
            d_row = {}
            for y, freq in sorted(row.iteritems()):
                d_row[y] = freq
            d[x] = d_row
        return d

    def prob_grid(self):
        if not self._prob_grid:
            self._prob_grid = {}
            for x, row in self.matrix.iteritems():
                prob_row = []
                for y, freq in sorted(row.iteritems()):
                    cum_freq = freq + sum(map(itemgetter(1), prob_row))
                    prob_row.append((y, cum_freq))
                    self._prob_grid[x] = prob_row

        return self._prob_grid

    def gen(self):
        grid = self.prob_grid()
        chain = deque(self.bootstrap(), maxlen=self.order)
        
        while True:
            key = tuple(chain)

            if not key in grid:
                chain = deque(self.bootstrap(), maxlen=self.order)

            row = grid[tuple(chain)]
            _, max_freq = row[-1]
            rnd = random.uniform(0, max_freq)

            n = next(val for val, cutoff in row if cutoff >= rnd)
            chain.append(n)
            yield n

    def bootstrap(self):
        return list(random.choice(self.prob_grid().keys()))
        # grid = self.prob_grid()
        # keys = grid.keys()
        # rnd = lambda: grid[random.choice(keys)]
        # return map(itemgetter(0), [rnd() for _ in xrange(length)])

    def seed(self):
        self.last_value = random.choice(self.prob_grid().keys())
        print self.last_value


class PitchMarkov(Markov):
    def get_features(self, thing):
        if type(thing) == music.chord.Chord:
            return tuple(p.midi for p in thing.pitches)
        elif type(thing) == music.note.Note:
            return tuple([thing.midi])

    def modify_note(self, _, pitches):
        return music.chord.Chord(pitches)

    def __str__(self):
        f = lambda x: tuple(str(music.note.Note(p).pitch) for p in x)
        g = lambda (notes, freq): (f(notes), freq)
        x = {tuple(map(f, k)): map(g, v) for k, v in self.prob_grid().iteritems()}
        return pprint.pformat(x, width=160)


class DurationMarkov(Markov):
    def get_features(self, thing):
        if type(thing) in (music.chord.Chord, music.note.Note):
            return thing.quarterLength

    def modify_note(self, thing, val):
        thing.quarterLength = val
        return thing

    def __str__(self):
        names = {8: 'double whole',
                 4: 'whole',
                 2: 'half',
                 1: 'quarter',
                 0.5: 'eighth',
                 0.25: 'sixteenth',
                 (1/6): 'triplet',
                 (1/3): 'quarter triplet'}
        dotted_names = {k*1.5: 'dotted ' + v for k, v in names.items()}
        names = dict(names.items() + dotted_names.items())

        f = lambda x: names[x] if x in names else x
        g = lambda (duration, freq): (f(duration), freq)
        x = {tuple(map(f, k)): map(g, v) for k, v in self.prob_grid().iteritems()}
        return pprint.pformat(x)


def rank(tune, markov):
    d = markov.to_dict()
    
    rank = 0
    for i, n in enumerate(tune[1:]):
        last = tuple([tune[i-1]])
        if last in d and n in d[last]:
            rank += d[last][n]

    return rank

def gen_random(length, markov):
    return [random.choice(sum((x.keys() for x in markov.to_dict().values()), []))
            for _ in range(length)]

def extend(d1, d2):
    return dict(d1.items() + d2.items())

def run_zoo(length, markov, **kwargs):
    opts = extend({'keep': 10,
                   'founders': 10,
                   'mutations': 2,
                   'spawns': 5,
                   'mutation_rate': 0.5,
                   'generations': 10}, kwargs)

    genomes = [gen_random(length, markov) for _ in xrange(opts['founders'])]
    genes = sum((x.keys() for x in markov.to_dict().values()), [])

    f = lambda g: rank(g, markov)
    
    for _ in range(opts['generations']):
        genomes.sort(key=f)
        survivors = genomes[:opts['keep']]
        genomes = list(survivors)
        for i, s in enumerate(survivors):
            for _ in range(opts['mutations']):
                split = random.randint(0, len(s)-1)
                mate = random.choice(survivors[:i] + survivors[i+1:])
                genomes.append(s[:split] + mate[split:])
                genomes.append(mate[:split] + s[split:])

            for _ in range(opts['spawns']):
                spawn = list(s)
                while random.random() < opts['mutation_rate']:
                    spawn[random.randint(0, len(spawn)-1)] = random.choice(genes)
                genomes.append(spawn)
            
    genomes.sort(key=f)
    return {f(x): x for x in genomes}


def gen_tune(length, pipeline):
    tune = music.stream.Stream()
    gens = {mkv: mkv.gen() for mkv in pipeline}

    for _ in range(length):
        n = music.note.Note()

        for mkv, gen in gens.iteritems():
            n = mkv.modify_note(n, gen.next())

        tune.append(n)

    return tune


def main(in_file, out_file, out_length, order):
    score = music.converter.parse(in_file)
    out_length = int(out_length)
    order = int(order)

    if not out_file.endswith('.midi'):
       out_file += '.midi'

    # TODO: select parts via interface
    notes = score.flat

    pitches = PitchMarkov(order, notes)
    durations = DurationMarkov(order, notes)

    pipeline = [pitches, durations]

    tune = gen_tune(out_length, pipeline)

    print list(tune)
    print pitches
    print durations

    tune.write('midi', out_file)

def prompt(msg):
    print msg
    return raw_input()

if __name__ == '__main__':
    if len(sys.argv) >= 5:
        main(*sys.argv[1:])
    else:
        main(prompt('What midi file do you want to train with?'),
             prompt('What do you want to call the output file?'),
             prompt('How many notes do you want to generate?'),
             prompt('Of what order should the markov chains be?'))
